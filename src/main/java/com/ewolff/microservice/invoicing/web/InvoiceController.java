package com.ewolff.microservice.invoicing.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;

import com.ewolff.microservice.invoicing.InvoiceRepository;

@Controller
public class InvoiceController {

	private InvoiceRepository invoiceRepository;

	@Autowired
	public InvoiceController(InvoiceRepository invoiceRepository, final MeterRegistry registry) {
		this.invoiceRepository = invoiceRepository;

		Gauge
            .builder("service_invoices_total", invoiceRepository, InvoiceRepository::count)
			.description("Total invoices")
			.tag("metric_type", "business")
            .register(registry);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
	public ModelAndView invoice(@PathVariable("id") long id) {
		return new ModelAndView("invoice", "invoice", invoiceRepository.findById(id).get());
	}

	@RequestMapping("/")
	public ModelAndView invoiceList() {
		return new ModelAndView("invoicelist", "invoices", invoiceRepository.findAll());
	}

}
